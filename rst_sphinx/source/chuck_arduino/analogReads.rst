.. _analogReads:

Interpretando sensores analógicos en ChucK desde Arduino
========================================================

Montajes
++++++++

Lectura de Potenciometro
------------------------

.. image:: img/potenciometer_bb.png

Código de ejemplo
+++++++++++++++++

Código ChucK
------------

.. literalinclude:: analog_reads/analogSensors.ck
   :linenos:


Código Arduino
--------------

.. literalinclude:: analog_reads/analog_reads.ino
   :language: c
   :emphasize-lines: 2
   :linenos:


