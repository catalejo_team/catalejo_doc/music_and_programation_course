.. _digitalReads:

Interpretando sensores digitales en ChucK desde Arduino
=======================================================

El siguiente ejemplo permite conectar diferentes sensores a Arduino digitales y ser interpretados por ChucK.
La cantidad de sensores, dependerá en primera instancia de las capacidades del Arduino.

Los montajes mostrados a continuación pueden hacer uso de los mismo códigos de programación.

Si se requiere extensiones (como aumentar la cantidad de sensores) los cambios en el código relativamente
sencillos de hacer.

Montajes
++++++++

Láser y foto-sensor
-------------------

.. image:: img/photo_sensor_and_laser_bb.png

Micrófono digital
-----------------

.. image:: img/microphone_positive_logic_bb.png

Interruptores en lógica negativa
--------------------------------

.. image:: img/switch_negative_logic_bb.png

Interruptores en lógica positiva
--------------------------------

.. image:: img/switch_positive_logic_bb.png

Código de ejemplo
+++++++++++++++++

Código ChucK
------------

.. code-block:: sh

   /*
    * @file photo_sensor_and_laser.ck
    * @brief Permite capturar dese el puerto serial diferentes lecturas
    *  de sensores digitales e interpretar desde ChucK.
    * @author: Johnny Cubides
    * contact: catalejoeducacion@gmail.com, jgcubidesc@gmail.com
    * date: Oct 03, 2018
    * license: GPL
    */
   //*********** CONFIGURACION PUERTO SERIAL *************
   <<<"Listando puertos seriales">>>;
   SerialIO.list() @=> string list[];
   for(int i; i < list.size(); i++){
      <<< i, ":", list[i] >>>;
   }
   <<<"Iniciando puerto serial">>>;
   SerialIO arduinoCom;
   //Seleccionar el puerto serial de interés
   //con el índice de la lista anterior
   arduinoCom.open(0, SerialIO.B9600, SerialIO.ASCII);

   1 => int MAX_DIGITAL_SENSOR;
   SinOsc osc[MAX_DIGITAL_SENSOR];
   [60 ] @=> int midi[];
   [0.5] @=> float volumen[];

   for(0 => int i; i< MAX_DIGITAL_SENSOR; i++)
   {
       osc[i] => dac;
       midi[i] => Std.mtof => osc[i].freq;
       0 => osc[i].gain;
   }

   // Variables
   string line; // Guarda los valores recogidos por el puerto serial
   int valueSensors; // Guarda el valor de los sensores
   50::ms => dur myTime; // Cada cuanto se actualizarán los sonidos

   fun void readSerial()
   {
       while(true)
       {
           arduinoCom.onLine() => now;
           arduinoCom.getLine() => line;
           if(line == "D")
           {
               arduinoCom.onLine() => now;
               arduinoCom.getLine() => line;
               Std.atoi(line) => valueSensors;
           }
       }
   }

   fun void playSond()
   {
       while(true)
       {
           for(0 => int i; i< MAX_DIGITAL_SENSOR; i++)
           {
               volumen[i]*(valueSensors & (1 << i)) => osc[i].gain;
           }
           myTime => now;
       }
   }

   spork ~ readSerial();
   spork ~ playSond();

   while(true) 1::second => now;

Código Arduino
--------------

.. code-block:: c

   /*
    * @file digital_reads.ino
    * @brief Permite hacer diferentes lecturas digitales optimizandolas y
    *  enviándolas por puerto serial
    * @author: Johnny Cubides
    * contact: catalejoeducacion@gmail.com, jgcubidesc@gmail.com
    * date: Oct 02, 2018
    * license: GPL
    *
    * Peso dado a cada sensor
    * 128
    * |64
    * ||32
    * |||16
    * ||||8421
    * ||||||||
    * 76543210 --> Posición de cada sensor
    * ||||||||_ primer sensor
    * |||||||__ segundo sensor
    * ||||||___ tercer sensor
    * |||||____ Cuarto sensor
    * ||||_____ Quinto sensor
    * |||______ Sexto sensor
    * ||_______ Séptimo sensor
    * |________ Octavo sensor
    *
    * Por cada sensor actico, (un uno lógico), al digital_value se suma un peso.
    * Ejemplo:
    *  pimer sensor activo         --> digital_value = 1
    *  segundo sensor activo       --> digital_value = 2
    *  tercer sensor activo        --> digital_value = 4
    *  los tres sensores activos   --> digital_value = 4+2+1 = 7
    *  Ocho sensores activos       --> digital_value = 128+64+32+16+8+4+2+1 = 255
    * Nota:
    *  No hay ninguna combinación que permita obtener el mismo resultado, es así
    *  que se puede distinguir qué sensor o qué sensores están activos.
    */
   // Máxima cantidad de sensores digitales a conectar
   #define MAX_DIGITAL_SENSOR 1 //Poner la cantidad que usted vaya a usar
   #define BAUDRATE 9600 //9600 o 115200
   #define DELAY 1 //Tiempo en milisegundos
   #define INITIAL 0

   // Definición de sensores: pin, valor digital, valor digital antiguo
   struct Digital_Sensor{
       int pin_number[MAX_DIGITAL_SENSOR];
       bool logic[MAX_DIGITAL_SENSOR];
       int digital_value;
       int digital_value_old;
   };

   // Creación de sensores digitales; tenga en cuenta la lógica del sensor
   struct Digital_Sensor digital_sensors = {
       // Poner el número de pin de los sensores según el orden que usted haya
       // establecido; la cantidad de pines debe coincidir con el MAX_DIGITAL_SENSOR.
       {2},
       {1}, //Lógica: 0 -> positiva; 1 -> negativa
       INITIAL,
       INITIAL
   };

   // Enviar lecturas digitales
   void send_digital_readings(void);

   void setup()
   {
       // Configuración de pines como entradas de información de sensores
       for(int i=0; i < MAX_DIGITAL_SENSOR; i++)
       {
           pinMode(digital_sensors.pin_number[i], INPUT);
       }
       // Velocidad de comunicación con el PC
       Serial.begin(BAUDRATE);
   }

   // Ejecución de programa principal
   void loop()
   {
       send_digital_readings();
   }

   void send_digital_readings(void)
   {
       // Lectura de sensores digitales
       for (int i = 0; i < MAX_DIGITAL_SENSOR; i++)
       {
          digital_sensors.digital_value |= ((int)(digital_sensors.logic[i]^digitalRead(digital_sensors.pin_number[i])))<<i;
       }
       // Envío de lectura digital
       if(digital_sensors.digital_value != digital_sensors.digital_value_old){
           // Si tiene solo éstas entradas digitales, puede omitir la siguiente línea comentandola
           Serial.println("D");
           // Enviando el valor de los sensores digitales
           Serial.println(digital_sensors.digital_value);
           digital_sensors.digital_value_old = digital_sensors.digital_value;
       }
       // Fin de transmisión de un paquete
       digital_sensors.digital_value = 0;
       delay(DELAY);
   }

