.. _external_led:

Encendido de LEDs externos
==========================

Blink de un LED
+++++++++++++++

.. todo::

   Teniendo en cuenta el ejemplo de uso :ref:`arduino_hello_word` monte el circuito
   mostrado a continuación y encienda y apague el LED con las modificaciones pertinentes
   del :ref:`example_code_hello_word`:

.. image:: img/blink_leds_externals_bb.png

Blink de dos LEDs
+++++++++++++++++

.. todo::

   Monte el circuito mostrado a continuación y logre los siguientes objetivos haciendo las
   modificaciones al codigo construido por usted anteriormente:

   * Encienda los dos LEDs a la vez durante medio segundo para luego apagarlo por medio segundo
     de manera cíclica (repetitiva).

   * Encienda un LED mientras el otro permanece apagado por medio segundo, a continuación, apague
     el primer LED para encender el otro LED durante medio segundo, hágalo de manera cíclica.

.. image:: img/blink_two_leds_externals_bb.png
