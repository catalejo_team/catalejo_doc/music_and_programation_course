Condiciones Lógicas
===================

Las condiciones lógicas dan lugar a preposiciones que al ser evaluadas
dan como resultado un valor verdadero (**true**) o falso (**false**).

Operadores relacionales
+++++++++++++++++++++++

   +---------+---------------------+---------------+
   | Símbolo | Significado         | Ejemplo       |
   +=========+=====================+===============+
   |  ==     | Es igual a          | if (x==0)     |
   +---------+---------------------+---------------+
   |  !=     | No es igual         | if (x != 0 )  |
   +---------+---------------------+---------------+
   |  <      | Menor que           | if (x < y )   |
   +---------+---------------------+---------------+
   |  >      | Mayor que           | if (x > y )   |
   +---------+---------------------+---------------+
   |  >=     | Mayor que o igual a | if (x >= y)   |
   +---------+---------------------+---------------+
   |  <=     | Menor que o igual a | if (x <= y)   |
   +---------+---------------------+---------------+

Estructuras de control de flujo
+++++++++++++++++++++++++++++++

Dependiendo del resultado de la evaluación de una preposición,
el programa tendrá un flujo u otro en su ejecución.

if
--

Diagrama de flujo
*****************

.. image:: img/if.png

Si una condición es verdadera, ejecutará lo que se encuentre dentro de
los corchetes

.. code-block:: c

   if(condicion){
      //Al ser verdadera la condicion se ejecuta lo que está aquí adentro
      //cuando termina ésta ejecución continua con el flujo normal del programa.
   }
   //Continúa con los demás procesos

Ejemplo
*******

.. code-block:: c

   //Habilitar unidad generadora seno y
   //conectada al conversor digital a análogo
   SinOsc miOsc => dac;
   //Volumen a la mitad
   0.5 => miOsc.gain;
   //Creación de una duración
   1.0 :: second => dur tiempo;
   //Nota a tocar
   //Cambiar por 60, 61
   60 => int nota;
   // Nota es igual a 60
   if(nota == 60){
       <<<"Verdadero, nota es igual a 60">>>;
       60 => Std.mtof => miOsc.freq, tiempo => now;
   }
   <<<"continúa con los demás procesos">>>;

else
----

Diagrama de flujo
*****************

.. image:: img/else.png

Si una condición es falsa, ejecutara lo que está después del **else** dentro
de los corchetes.

.. code-block:: c

   if(condicion){
      //Al ser la condición falsa, el programa evita entrar a éste ambito
      //señalado por los corchetes
   }else{
      //Al ser falsa la condicion,
      //el programa ejecutará lo que está dentro de éstos corchetes.
   }
   //Continúa con los demás procesos

Ejemplo
*******

.. code-block:: c

   //Habilitar unidad generadora seno y
   //conectada al conversor digital a análogo
   SinOsc miOsc => dac;
   //Volumen a la mitad
   0.5 => miOsc.gain;
   //Creación de una duración
   1.0 :: second => dur tiempo;
   //Nota a tocar
   //Cambiar por 60, 61
   61 => int nota;
   // Nota es igual a 60
   if(nota == 60){
       <<<"Verdadero, nota es igual a 60">>>;
       60 => Std.mtof => miOsc.freq, tiempo => now;
   }else{
       <<<"La anterior condición fue falsa">>>;
   }
   <<<"continúa con los demás procesos">>>;

else if
-------

Diagrama de flujo
*****************

.. image:: img/elseif.png

Si la condición anterior es falsa, el programa evaluará la siguiente condición
que se encuentra dentro de los paréntesis después del **else if**. Si ésta
condición es verdadera, ejecutará lo que está dentro de los corchetes después
de dicha condición, finalizada la ejecución, ya no entra a evaluar más
condiciones y continúa su flujo normal. Si la condición es falsa, continúa
evaluando las demás condiciones hasta encontrar una verdadera o hasta llegar
al **else** que indicaría que ninguna condición fue verdadera.

.. code-block:: c

   if(condicion){
      //Al ser la condición falsa, el programa evita entrar a éste ambito
      //y prueba evaluar la siguiente condición
   }else if(condicion){
      //Si la condición es verdadera, ejecuta lo que está dentro de
      //éstos corchetes.
   else{
      //Si ninguna condición fue verdadera, entonces,
      //el programa ejecutará lo que está dentro de éstos corchetes.
   }
   //Continúa con los demás procesos


Ejemplo
*******

.. code-block:: c

   //Habilitar unidad generadora seno y
   //conectada al conversor digital a análogo
   SinOsc miOsc => dac;
   //Volumen a la mitad
   0.5 => miOsc.gain;
   //Creación de una duración
   1.0 :: second => dur tiempo;
   //Nota a tocar
   //Cambiar por 60, 61, 62, 63
   61 => int nota;
   // Nota es igual a 60
   if(nota == 60){
       <<<"Verdadero, nota es igual a 60">>>;
       60 => Std.mtof => miOsc.freq, tiempo => now;
   }else if(nota == 61){
       <<<"Verdadero, nota es igual a 61">>>;
       61 => Std.mtof => miOsc.freq, tiempo => now;
   }else if(nota == 62){
       <<<"Verdadero, nota es igual a 62">>>;
       62 => Std.mtof => miOsc.freq, tiempo => now;
   }else{
       <<<"Todas las condiciones fueron falsas">>>;
   }
   <<<"continúa con los demás procesos">>>;
