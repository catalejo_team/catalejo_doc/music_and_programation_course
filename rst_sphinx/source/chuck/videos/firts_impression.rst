Primera impresión
=================

.. _aquí en video: https://youtu.be/9TomYHLYTvg

.. raw:: html

   <iframe width="560" height="315" src="https://www.youtube.com/embed/9TomYHLYTvg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

.. attention::
   **¿No puedes ver el vídeo?** Mantén presionado `aquí en video`_ hasta que se habilite la opción de abrirlo en la app de YOUTUBE.

.. note::
   La salida por consola de texto, permite ver que parte del código se está ejecutando si se usa correctamente.

Código del ejemplo
++++++++++++++++++

.. code-block:: c

   <<<"Hola mundo">>>;

.. todo::
   Prueba agregando más lineas de texto en la consola con <<<"poner aquí el texto">>>; (sin olvidar el punto y coma).
