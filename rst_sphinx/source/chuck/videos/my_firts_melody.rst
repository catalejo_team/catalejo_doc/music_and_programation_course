Mi primera melodía
==================

.. hint::
   Antes de ver el vídeo ten presente la información alojada en los siguientes links:

   * :ref:`duration`.

   * :ref:`octave_and_midi_numbers`.

   * :ref:`musescore_interest`.


.. _aquí en video: https://youtu.be/X2dxPIo4NXA

.. raw:: html

   <iframe width="560" height="315" src="https://www.youtube.com/embed/X2dxPIo4NXA" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

.. attention::
   **¿No puedes ver el vídeo?** Mantén presionado `aquí en video`_ hasta que se habilite la opción de abrirlo en la app de YOUTUBE.

Código del ejemplo:
+++++++++++++++++++

...Próximamente... :D

.. todo::
   Haz tu propia melodía escogiendo una partitura sencilla.
