Mi propio amplificador de audio
===============================

.. _aquí en video: https://youtu.be/-pyexnjNfM0

.. raw:: html

   <iframe width="560" height="315" src="https://www.youtube.com/embed/-pyexnjNfM0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

.. attention::
   **¿No puedes ver el vídeo?** Mantén presionado `aquí en video`_ hasta que se habilite la opción de abrirlo en la app de YOUTUBE.

