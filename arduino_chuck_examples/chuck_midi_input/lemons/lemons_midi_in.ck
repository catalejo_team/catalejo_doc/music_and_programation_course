// protocolo midi
// [note on/off, notamidi, volumen nota]
// [144:90, 0:128, 0:127]
90 => int noteOff; // definición de la nota en ON
144 => int noteOn; // definición de la nota en OFF
// fuerza con la que se toca la nota
1 => int vel;

// selección de instrumento
//
Rhodey instrumento[6];
instrumento[0] => dac;
instrumento[1] => dac;
instrumento[2] => dac;
instrumento[3] => dac;
instrumento[4] => dac;
instrumento[5] => dac;

// selección de notas
[69,60,60,70,70,70] @=> int notas[];

// asignación de notas a cada elemento del instrumento
for( 0 => int i; i < 5; i++ ){
  Std.mtof(notas[i]) => instrumento[i].freq;
}


// configuración del puerto Midi
MidiIn min;
1 => int port;

if( !min.open(port) )
{
  <<< "Error: MIDI port did not open on port: ", port >>>;
  me.exit();
}
// Mensajes Midi
MidiMsg msg;

fun int play_note(int tecla, int state){
  //<<<"aqui">>>;
  //<<<state>>>;
  if(state == noteOn){
    vel => instrumento[tecla].noteOn;
  }else{
    //vel => instrumento[tecla].noteOff;
  }
  return 0;
}

while( true ){
  min => now; // advance when receive MIDI msg
  while( min.recv(msg) )
  {
    // <<< msg.data1, msg.data2, msg.data3 >>>;
    if (msg.data2 == 60) {
      play_note(0, msg.data1);
    }else if(msg.data2 == 61){
      play_note(1, msg.data1);
    }else if(msg.data2 == 62){
      play_note(2, msg.data1);
    }else if(msg.data2 == 63){
      play_note(3, msg.data1);
    }else if(msg.data2 == 64){
      play_note(4, msg.data1);
    }else if(msg.data2 == 65){
      play_note(5, msg.data1);
    }
  }
}

