1.0 :: second => dur compas;
compas/2 => dur N; //Negra
4*N => dur R; //Redonda
2*N => dur B; //Blanca
N/2 => dur C; //Corchea
N/4 => dur S; //Semicorchea
N/8 => dur F; //Fusa
N/16 => dur SF; //Semifusa
SinOsc miOsc => dac;
<<<"Compas 1">>>;
60 => Std.mtof => miOsc.freq, 0.0 => miOsc.gain, N => now;
71 => Std.mtof => miOsc.freq, 0.5 => miOsc.gain, S => now;
69 => Std.mtof => miOsc.freq, 0.5 => miOsc.gain, S => now;
68 => Std.mtof => miOsc.freq, 0.5 => miOsc.gain, S => now;
69 => Std.mtof => miOsc.freq, 0.5 => miOsc.gain, S => now;
<<<"Compas 2">>>;
72 => Std.mtof => miOsc.freq, 0.5 => miOsc.gain, N => now;

74 => Std.mtof => miOsc.freq, 0.5 => miOsc.gain, S => now;
72 => Std.mtof => miOsc.freq, 0.5 => miOsc.gain, S => now;
71 => Std.mtof => miOsc.freq, 0.5 => miOsc.gain, S => now;
72 => Std.mtof => miOsc.freq, 0.5 => miOsc.gain, S => now;
<<<"Compas 3">>>;
76 => Std.mtof => miOsc.freq, 0.5 => miOsc.gain, N => now;

77 => Std.mtof => miOsc.freq, 0.5 => miOsc.gain, S => now;
76 => Std.mtof => miOsc.freq, 0.5 => miOsc.gain, S => now;
75 => Std.mtof => miOsc.freq, 0.5 => miOsc.gain, S => now;
76 => Std.mtof => miOsc.freq, 0.5 => miOsc.gain, S => now;
<<<"Compas 4">>>;
83 => Std.mtof => miOsc.freq, 0.5 => miOsc.gain, S => now;
81 => Std.mtof => miOsc.freq, 0.5 => miOsc.gain, S => now;
80 => Std.mtof => miOsc.freq, 0.5 => miOsc.gain, S => now;
81 => Std.mtof => miOsc.freq, 0.5 => miOsc.gain, S => now;

83 => Std.mtof => miOsc.freq, 0.5 => miOsc.gain, S => now;
81 => Std.mtof => miOsc.freq, 0.5 => miOsc.gain, S => now;
80 => Std.mtof => miOsc.freq, 0.5 => miOsc.gain, S => now;
81 => Std.mtof => miOsc.freq, 0.5 => miOsc.gain, S => now;
<<<"Compas 5">>>;
84 => Std.mtof => miOsc.freq, 0.5 => miOsc.gain, N => now;

81 => Std.mtof => miOsc.freq, 0.5 => miOsc.gain, C => now;
84 => Std.mtof => miOsc.freq, 0.5 => miOsc.gain, C => now;
<<<"Compas 6">>>;
83 => Std.mtof => miOsc.freq, 0.5 => miOsc.gain, C => now;
81 => Std.mtof => miOsc.freq, 0.5 => miOsc.gain, C => now;
79 => Std.mtof => miOsc.freq, 0.5 => miOsc.gain, C => now;
81 => Std.mtof => miOsc.freq, 0.5 => miOsc.gain, C => now;
<<<"Compas 7">>>;
83 => Std.mtof => miOsc.freq, 0.5 => miOsc.gain, C => now;
81 => Std.mtof => miOsc.freq, 0.5 => miOsc.gain, C => now;
79 => Std.mtof => miOsc.freq, 0.5 => miOsc.gain, C => now;
81 => Std.mtof => miOsc.freq, 0.5 => miOsc.gain, C => now;
<<<"Compas 8">>>;
83 => Std.mtof => miOsc.freq, 0.5 => miOsc.gain, C => now;
81 => Std.mtof => miOsc.freq, 0.5 => miOsc.gain, C => now;
79 => Std.mtof => miOsc.freq, 0.5 => miOsc.gain, C => now;
78 => Std.mtof => miOsc.freq, 0.5 => miOsc.gain, C => now;
<<<"Compas 9">>>;
76 => Std.mtof => miOsc.freq, 0.5 => miOsc.gain, N => now;