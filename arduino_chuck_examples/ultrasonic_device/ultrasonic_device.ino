#include <NewPing.h>

//Pin donde se pondrá el disparador del ultrasonido
#define TRIGGER_PIN 3
//Pin donde se pondrá el eco
#define ECHO_PIN 2
//Definir máxima distancia en centímetros
#define MAX_DISTANCE 100

//Creación de configuración para ultrasonido
NewPing sensor(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE);

void setup(){
    //Inicia la comunicación con el computador
    //Velocidad:
    //  lenta:  9600 baudios
    //  alta:   115200 baudios
    Serial.begin(9600);
}

// Inicio de programa pincipal
void loop(){
    // Cada 100ms se hace una toma de la distancia
    delay(100);
    //Se envía el valor leído del sensor al computador
    Serial.println(sensor.ping_cm());
}
