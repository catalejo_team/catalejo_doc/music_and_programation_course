/*
 * @file analogSensors.ck
 * @brief Permite capturar dese el puerto serial diferentes lecturas
 *  de sensores analógicos e interpretar desde ChucK.
 * @author: Johnny Cubides
 * contact: catalejoeducacion@gmail.com, jgcubidesc@gmail.com
 * date: Oct 03, 2018
 * license: GPL
 */
//*********** CONFIGURACION PUERTO SERIAL *************
<<<"Listando puertos seriales">>>;
SerialIO.list() @=> string list[];
for(int i; i < list.size(); i++){
   <<< i, ":", list[i] >>>;
}
<<<"Iniciando puerto serial">>>;
SerialIO arduinoCom;
//Seleccionar el puerto serial de interés
//con el índice de la lista anterior
arduinoCom.open(0, SerialIO.B9600, SerialIO.ASCII);

3 => int MAX_ANALOG_SENSOR; //Cantidad de sensores
3 => int MAX_OSCILATORS; //Cantidad de osciladores simultaneos a usar

//Creación de osciladores
SinOsc osc_a[MAX_OSCILATORS];
[60 ,  61,  62] @=> int midi_a[]; //Corresponde a la cantidad de osciladores a controlar
[0.5, 0.5, 0.5] @=> float volumen_a[]; //Corresponde a la cantidad de osiladores a controlar

//Iniciación de osciladores
for(0 => int i; i< MAX_OSCILATORS; i++)
{
    osc_a[i] => dac;
    midi_a[i] => Std.mtof => osc_a[i].freq;
    0 => osc_a[i].gain;
}

// Variables
string line; // Guarda los valores recogidos por el puerto serial
//el siguiente arreglo debe coincidir con el numbre y la cantidad
//de identificadores para los sensores
["S1", "S2", "S3"]@=> string analogSensors[];
int valueAnalogSensors[MAX_ANALOG_SENSOR]; // Guarda el valor de los sensores
50::ms => dur myTime; // Cada cuanto se actualizarán los sonidos

fun void readSerial()
{
    while(true)
    {
        arduinoCom.onLine() => now;
        arduinoCom.getLine() => line;
        for(0=>int i; i < MAX_ANALOG_SENSOR; i++)
        {
            if(line == analogSensors[i])
            {
                arduinoCom.onLine() => now;
                arduinoCom.getLine() => line;
                Std.atoi(line) => valueAnalogSensors[i];
                break;
            }
        }
    }
}

fun void playSond()
{
    while(true)
    {
        valueAnalogSensors[0] => osc_a[0].freq;
        0.1 => osc_a[0].gain;
        myTime => now;
    }
}

spork ~ readSerial();
spork ~ playSond();

while(true) 1::second => now;

