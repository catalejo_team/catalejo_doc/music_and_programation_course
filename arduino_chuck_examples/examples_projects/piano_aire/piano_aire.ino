#include <CapacitiveSensor.h>

/*
 * CapitiveSense Library Demo Sketch
 * Paul Badger 2008
 * Uses a high value resistor e.g. 10M between send pin and receive pin
 * Resistor effects sensitivity, experiment with values, 50K - 50M. Larger resistor values yield larger sensor values.
 * Receive pin is the sensor pin - try different amounts of foil/metal on this pin
 */
#define speaker 13

CapacitiveSensor   cs_2_3 = CapacitiveSensor(2,3);        // conectores de resistencia en un pin 2 la patita como actuara como emisor y en el 5 como receptor
CapacitiveSensor   cs_2_4 = CapacitiveSensor(2,4);        // conectores de resistencia en un pin 2 la patita como actuara como emisor y en el 6 como receptor
CapacitiveSensor   cs_2_5 = CapacitiveSensor(2,5);        // conectores de resistencia en un pin 2 la patita como actuara como emisor y en el 7 como receptor
CapacitiveSensor   cs_2_6 = CapacitiveSensor(2,6);        // conectores de resistencia en un pin 2 la patita como actuara como emisor y en el 8 como receptor
CapacitiveSensor   cs_2_7 = CapacitiveSensor(2,7);        // conectores de resistencia en un pin 2 la patita como actuara como emisor y en el 9 como receptor
CapacitiveSensor   cs_2_8 = CapacitiveSensor(2,8);        // conectores de resistencia en un pin 2 la patita como actuara como emisor y en el 10 como receptor
CapacitiveSensor   cs_2_9 = CapacitiveSensor(2,9);        // conectores de resistencia en un pin 2 la patita como actuara como emisor y en el 11 como receptor

void setup(){
  // iniciamos el puerto serial para poder mostrar los resultados en la computadora
   Serial.begin(9600);
}

void loop()
{
    long start = millis();
    long total1 =  cs_2_3.capacitiveSensor(30);
    long total2 =  cs_2_4.capacitiveSensor(30);
    long total3 =  cs_2_5.capacitiveSensor(30);
    long total4 =  cs_2_6.capacitiveSensor(30);
    long total5 =  cs_2_7.capacitiveSensor(30);
    long total6 =  cs_2_8.capacitiveSensor(30);
    long total7 =  cs_2_9.capacitiveSensor(30);

    Serial.print(millis() - start);        // check on performance in milliseconds
    Serial.print("\t");                    // tab character for debug windown spacing
    Serial.print(total1);                  // print sensor output 1
    Serial.print("\t");
    Serial.print(total2);                  // print sensor output 2
    Serial.print("\t");
    Serial.print(total3);                  // print sensor output 3
    Serial.print("\t");
    Serial.print(total4);                  // print sensor output 4
    Serial.print("\t");
    Serial.print(total5);                  // print sensor output 5
    Serial.print("\t");
    Serial.print(total6);                  // print sensor output 6
    Serial.print("\t");
    Serial.print(total7);                  // print sensor output 7
    Serial.print("\t");
    Serial.print("\n");

 if (total1 > 1000) tone(speaker,523);// colocamos que si cumple la condicion en la salida del pin 13 o  de la bocina que nos reprodusca un sonido similar al de DO
 if (total2 > 1000) tone(speaker,587);// colocamos que si cumple la condicion en la salida del pin 13 o  de la bocina que nos reprodusca un sonido similar al de RE
 if (total3 > 1000) tone(speaker,659);// colocamos que si cumple la condicion en la salida del pin 13 o  de la bocina que nos reprodusca un sonido similar al de MI
 if (total4 > 700) tone(speaker,698);// colocamos que si cumple la condicion en la salida del pin 13 o  de la bocina que nos reprodusca un sonido similar al de FA
 if (total5 > 900) tone(speaker,784);// colocamos que si cumple la condicion en la salida del pin 13 o  de la bocina que nos reprodusca un sonido similar al de SOL
 if (total6 > 900) tone(speaker,880);// colocamos que si cumple la condicion en la salida del pin 13 o  de la bocina que nos reprodusca un sonido similar al de LA
 if (total7 > 900) tone(speaker,988);// colocamos que si cumple la condicion en la salida del pin 13 o de la bocina que nos reprodusca un sonido similar al de SI

 if ((total1<=1000)&&(total2<=1000)&&(total3<=1000)&&(total4<=700)&&(total5<=900)&&(total6<=900)&&(total7<=900))
    noTone(speaker); // colocamos que si cumple la condicion este no emita ningun tipo de sonido
 }
